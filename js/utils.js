// put any utility functions here

var Utils = (function() {

	/**
	 * Find a search term in the (sorted) list of possible search terms
	 * @param terms        the input search terms
	 * @param allTerms     the sorted array we're searching against
	 * @return             the sorted list of all matching entries in allTerms
	 */
	function searchTerm(terms, allTerms) {
		// debugger;
		if(!terms || terms.length === 0 || !terms[0]) {
			console.log('empty');
			return [];
		}
		var i, results=[], j;
		for(i=0;i<allTerms.length;i++) {
			for(j=0;j<terms.length;j++) {
				if(allTerms[i].toLowerCase().indexOf(terms[j].toLowerCase()) >= 0) {
					results.push(allTerms[i]);
					break;
				}
			}
		}
		return results;
	}

	// removes a course from saved or registered list based on course code
	function removeCourseFromList(course, arr) {
		var code = course[Data.CRN],
			i;
		for(i=0;i<arr.length;i++) {
			if(arr[i][Data.CRN] === code) {
				arr.splice(i,1);
			}
		}
	}

	// finds a course in a list based on code
	function searchCourseInList(course, arr) {
		var code = course[Data.CRN], i;
		for(i=0;i<arr.length;i++) {
			if(arr[i][Data.CRN] === code) {
				return true;
			}
		}
		return false;
	}

	function mapToBlock(course) {
		var blocks = ['A', 'AB', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',
		              'J', 'K', 'L', 'L_', 'M', 'M_', 'N', 'N_',
		              'O', 'O_', 'P', 'P_', 'Q', 'Q_', 'R', 'S', 'T'];
		var name = course[Data.NAME];
		var i;
		var index = 0;
		for(i=0;i<name.length;i++) {
			index += name.charCodeAt(i);
			index %= blocks.length;
		}
		console.log(blocks[index]);
		return blocks[index];
	}

	/**
	A: MWF 8-8:50am
	AB: MW 8:30-9:50am
	B: MWF 9-9:50am
	C: MWF 10-10:50am
	D: MWF 11-11:50am
	E: MWF 12-12:50pm
	F: MWF 1-1:50pm 
	G: MWF 2-2:50pm
	H: TR 9-10:20am 
	I: 10:30-11:50
	J: 1-2:20pm
	K: 2:30-3:50pm
	L: TR 6:30-7:50pm
	L_: TR 6:40-8:00pm
	M: M 3-5:20
	M_: M 3-5:30 
	N: W 3-5:20
	N_: W 3-5:30
	O: F 3-5:20
	O_: F 3-5:30
	P: T 4-6:20
	P_: T 4-6:30
	Q: R 4-6:20
	Q_: R 4-6:30
	R: F 9:30-11:50
	S: W 6-8:20
	T: MW 3-4:20
	*/


	/**
	 * Takes a number from 0 to 23 and formats it as a time of day.
	 * @param num     number to format
	 * @return        formatted string
	 */
	function formatTime(num) {
		var meridian = (num >= 12) ? "pm" : "am";
		var floor = Math.floor(num);
		var hour = (floor !== 12 && floor !== 0) ? floor % 12 : 12;
		var mins = Math.round((num % 1) * 60);
		if(mins < 10) {
			mins = "0" + mins;
		}
		return hour + ":" + mins + meridian;
	}


	/**
	 * Create a table from some data
	 * @param data     array of objects storing course data
	 */
	function createTable(tableObj) {
	 	var i, j,
	 		table,
	 		numResults,
	 		tableHead,
	 		tableHeader,
	 		tableBody,
	 		startTime,
	 		tableEntry, text,
			data, headers, inputClass, numRows, numCols;
			
		data = tableObj.data || [];
		headers = tableObj.headers || [];
		inputClass = tableObj.inputClass;
		numRows = tableObj.numRows || 0;
		numCols = tableObj.numCols || 0;
		startTime = tableObj.startTime || 0;

		
		numResults = data.length > 0 ? data.length : numRows;
		numColumns = headers.length > 0 ? headers.length : numCols;
		
		console.log("numResults = " + numResults);

		if(numResults.length === 0) {
			return;
		}
	
	 	table = $(document.createElement('table'));
	 	table.attr({
	 		startTime: startTime,
	 		stopTime: startTime + numResults
	 	})
	 	table.addClass('createdTable');
		inputClass && table.addClass(inputClass);
		if(headers.length > 0) {
	 		tableHead = $(document.createElement('thead'));
		 	tableHead.addClass('tableHead');
		 	tableRow = $(document.createElement('tr'));
		 	tableHead.append(tableRow);
		 	for(i=0;i<numColumns;i++) {
		 		tableHeader = $(document.createElement('th'));
		 		tableRow.append(tableHeader);
		 		tableHeader.attr('id', headers[i]);
		 		tableHeader.text(headers[i]);
		 	}
		 	table.append(tableHead);
		}

	 	tableBody = $(document.createElement('tbody'));
	 	table.append(tableBody);
	 	for(i=0;i<numResults;i++) {
	 		tableRow = $(document.createElement('tr'));
	 		tableBody.append(tableRow);
	 		for(j=0;j<numColumns;j++) {
	 			tableEntry = $(document.createElement('td'));
	 			tableRow.append(tableEntry);
	 			tableEntry.css('width', (100/numColumns)+'%')
				if(j===0 && headers[0] === "") {
					tableEntry.css({
						'vertical-align':'text-top',
						'font-size':'90%'
					});
	 				text = formatTime(startTime + i);
	 			} else {
	 				text = data.length > 0 ? data[i][headers[j]] : '';
	 			}
	 			tableEntry.text(text);
	 		}
	 	}
	 	return table;
	}

	/**
	 * Uses input options to retrieve course info from the Banner API
	 * Uses the jQuery.ajax method to do this
	 * @param options      parameter object to be turned into query params
	 */
	function retrieveCourseData(options, callback, errorCallback) {

		options.output = 'json';
		$.ajax({
			url: 'http://www.brown.edu/web/john/course-metadata/index.php',
			dataType: 'jsonp',
			//data: options,
			success: callback,
			//error: errorCallback
		});
	}

	/**
	 * Takes a list of courses and returns an array of arrays, where the
	 * courses in each inner array share a time block
	 * @param courseList     the course array to sort
	 * DEPRECATED FUNCTION
	 */
	function processCourses(courseList) {
		var i=0,
		    j,
		    block,
		    ret = [],
		    curr = [],
		    course,
		    courses = courseList.slice(0);
		courses.sort(function(a,b){
			return a.Block < b.Block ? -1 : 1;
		});
		while(i < courses.length) {
			course = courses[i];
			curr = [course];
			i++;
			while(i < courses.length && courses[i].Block ===  course.Block) {
				curr.push(courses[i]);
				i++;
			}
			ret.push(curr);
		}
		return ret;
	}

	function getConflicts(course, arr) {
		var conflicting = [], i;
		//debugger;
		for(i=0;i<arr.length;i++){
			if(isConflicting(course, arr[i])){
				conflicting.push(arr[i]);
			}
		}
		return conflicting;
	}

	function isConflicting(course1, course2) {
		if(course1[Data.CRN] === course2[Data.CRN]) { // same course
			return false;
		}
		var hours1 = convertBlockHour(convertStringToBlock(course1[Data.SCHEDULE])),
		    hours2 = convertBlockHour(convertStringToBlock(course2[Data.SCHEDULE])),
		    i, j, k;
		
		for(i=0;i<5;i++) {
			for(j=0;j<hours1[i].length;j++) {
				for(k=0;k<hours2[i].length;k+=2) {
					if(hours1[i][j] >= hours2[i][k] && hours1[i][j] <= hours2[i][k+1]) {
						return true;
					}
				}
			}
		}

		return false;
	}

	function convertBlockToString(blockString){
		var block = "";

		if(blockString === "A"){
			block = "M W F 8-8:50";
		}
		else if(blockString === 'AB'){
			block = "M W 8:30-9:50";
		}
		else if(blockString === 'B'){
			block = "M W F 9-9:50";
		}
		else if(blockString === 'C'){
			block = "M W F 10-10:50";
		}
		else if(blockString === "D"){
			block = "M W F 11-11:50";
		}
		else if(blockString === "E"){
			block = "M W F 12-12:50";
		}
		else if(blockString === "F"){
			block = "M W F 1-1:50";
		}
		else if(blockString === "G"){
			block = "M W F 2-2:50";
		}
		else if(blockString === "H"){
			block = "T Th 9-10:20";
		}
		else if(blockString === "I"){
			block = "T Th 10:30-11:50";
		}
		else if(blockString === "J"){
			block = "T Th 1-2:20";
		}
		else if(blockString === "K"){
			block = "T Th 2:30-3:50";
		}
		else if(blockString === "L"){
			block = "T Th 6:30-7:50";
		}
		else if(blockString === "L_"){
			block = "T Th 6:40-8";
		}
		else if(blockString === "M"){
			block = "M 3-5:20";
		}
		else if(blockString === "M_"){
			block = "M 3-5:30";
		}
		else if(blockString === "N"){
			block = "W 3-5:20";
		}
		else if(blockString === "N_"){
			block = "W 3-5:30";
		}
		else if(blockString === "O"){
			block = "F 3-5:20";
		}
		else if(blockString === "O_"){
			block = "F 3-5:30";
		}
		else if(blockString === "P"){
			block = "T 4-6:20";
		}
		else if(blockString === "P_"){
			block = "T 4-6:30";
		}
		else if(blockString === "Q"){
			block = "Th 4-6:20";
		}
		else if(blockString === "Q_"){
			block = "Th 4-6:30";
		}
		else if(blockString === "R"){
			block = "F 9:30-11:50";
		}
		else if(blockString === "S"){
			block = "W 6-8:20";
		}
		else if(blockString === "T"){
			block = "M W 3-4:20";
		}
		return block;
	}

	function convertStringToBlock(searchString){
		var block = "";
		var blockString = searchString.replace(/[ \-\:]/g,"").toLowerCase();

		if(blockString === "mwf8850" || blockString === "mwf08000850"){
			block = "A";
		}
		else if(blockString === "mw830950" || blockString === "mw08300950"){
			block = 'AB';
		}
		else if(blockString === "mwf9950" || blockString === "mwf09000950"){
			block = 'B';
		}
		else if(blockString === "mwf101050" || blockString === "mwf10001050"){
			block = 'C';
		}
		else if(blockString === "mwf111150" || blockString === "mwf11001150"){
			block = "D";
		}
		else if(blockString === "mwf121250" || blockString === "mwf12001250"){
			block = "E";
		}
		else if(blockString === "mwf1150" || blockString === "mwf13001350"){
			block = "F";
		}
		else if(blockString === "mwf2250" || blockString === "mwf14001450"){
			block = "G";
		}
		else if(blockString === "tth91020" || blockString === "tr09001020"){
			block = "H";
		}
		else if(blockString === "tth10301150" || blockString === "tr10301150"){
			block = "I";
		}
		else if(blockString === "tth1220" || blockString === "tr13001420"){
			block = "J";
		}
		else if(blockString === "tth230350" || blockString === "tr14301550"){
			block = "K";
		}
		else if(blockString === "tth630750" || blockString === "tr18301950"){
			block = "L";
		}
		else if(blockString === "tth6408" || blockString === "tr18402000"){
			block = "L_";
		}
		else if(blockString === "m3520" || blockString === "m15001720"){
			block = "M";
		}
		else if(blockString === "m3530" || blockString === "m15001730"){
			block = "M_";
		}
		else if(blockString === "w3520" || blockString === "w15001720"){
			block = "N";
		}
		else if(blockString === "w3530" || blockString === "w15001730"){
			block = "N_";
		}
		else if(blockString === "f3520" || blockString === "f15001720"){
			block = "O";
		}
		else if(blockString === "f3530" || blockString === "f15001730"){
			block = "O_";
		}
		else if(blockString === "t4620" || blockString === "t16001820"){
			block = "P";
		}
		else if(blockString === "t4630" || blockString === "t16001830"){
			block = "P_";
		}
		else if(blockString === "th4620" || blockString === "r16001820"){
			block = "Q";
		}
		else if(blockString === "th4630" || blockString === "r16001830"){
			block = "Q_";
		}
		else if(blockString === "f9301150" || blockString === "f09301150"){
			block = "R";
		}
		else if(blockString === "w6820" || blockString === "w18002030"){
			block = "S";
		}
		else if(blockString === "mw3420" || blockString === "mw15001620"){
			block = "T";
		}
		else if(blockString === "mtwr111150" || blockString === "mtwr11001150") {
			block = "U";
		} else if (blockString.match(/[a-z][0-9][0-9][0-9][0-9]/)) {
			block = "K";
		}
		return block;
	}

	function convertBlockHour(block){
		var blockArray;
		if(block === "") {
			blockArray = [[],[],[],[],[]];
		}
		else if(block == "A"){
			blockArray = [[8, 8.833],[],[8, 8.833],[],[8, 8.833]];
			return blockArray;
		}
		else if(block == "AB"){
			blockArray = [[8.5, 9.833],[],[8.5, 9.833],[],[]];
			return blockArray;
		}
		else if(block == "B"){
			blockArray = [[9, 9.833],[],[9, 9.833],[],[9, 9.833]];
			return blockArray;
		}
		else if(block == "C"){
			blockArray = [[10, 10.833],[],[10, 10.833],[],[10, 10.833]];
			return blockArray;
		}
		else if(block == "D"){
			blockArray = [[11, 11.833],[],[11, 11.833],[],[11, 11.833]];
			return blockArray;
		}
		else if(block == "E"){
			blockArray = [[12, 12.833],[],[12, 12.833],[],[12, 12.833]];
			return blockArray;
		}
		else if(block == "F"){
			blockArray = [[13, 13.833],[],[13, 13.833],[],[13, 13.833]];
			return blockArray;
		}
		else if(block == "G"){
			blockArray = [[14, 14.833],[],[14, 14.833],[],[14, 14.833]];
			return blockArray;
		}
		else if(block == "H"){
			blockArray = [[],[9, 10.333],[],[9, 10.333],[]];
			return blockArray;
		}
		else if(block == "I"){
			blockArray = [[],[10.5, 11.833],[],[10.5, 11.833],[]];
			return blockArray;
		}
		else if(block == "J"){
			blockArray = [[],[13, 14.333],[],[13, 14.333],[]];
			return blockArray;
		}
		else if(block == "K"){
			blockArray = [[],[14.5, 15.833],[],[14.5, 15.833],[]];
			return blockArray;
		}
		else if(block == "L"){
			blockArray = [[],[18.5, 19.833],[],[18.5, 19.833],[]];
			return blockArray;
		}
		else if(block == "L_"){
			blockArray = [[],[14.667, 20],[],[14.667, 20],[]];
			return blockArray;
		}
		else if(block == "M"){
			blockArray = [[15, 17.333],[],[],[],[]];
			return blockArray;
		}
		else if(block == "M_"){
			blockArray = [[15, 17.5],[],[],[],[]];
			return blockArray;
		}
		else if(block == "N"){
			blockArray = [[],[],[15, 17.333],[],[]];
			return blockArray;
		}
		else if(block == "N_"){
			blockArray = [[],[],[15, 17.5],[],[]];
			return blockArray;
		}
		else if(block == "O"){
			blockArray = [[],[],[],[],[15, 17.333]];
			return blockArray;
		}
		else if(block == "O_"){
			blockArray = [[],[],[],[],[15, 17.5]];
			return blockArray;
		}
		else if(block == "P"){
			blockArray = [[],[16, 18.333],[],[],[]];
			return blockArray;
		}
		else if(block == "P_"){
			blockArray = [[],[16, 18.5],[],[],[]];
			return blockArray;
		}
		else if(block == "Q"){
			blockArray = [[],[],[],[16, 18.333],[]];
			return blockArray;
		}
		else if(block == "Q_"){
			blockArray = [[],[],[],[16, 18.5],[]];
		} else if(block == "R"){
			blockArray = [[],[],[],[],[9.5, 11.833]];
		} else if(block == "S"){
			blockArray = [[],[],[18, 20.33],[],[]];
		} else if(block == "T"){
			blockArray = [[15, 16.333],[],[15, 16.333],[],[]];
		} else if(block === "U") {
			blockArray = [[11,11.833],[11,11.833],[11,11.833],[11,11.833],[]];
		}
		return blockArray;
	}

	/**
	 * General purpose error handler
	 * @param err     an error object
	 */
	function errorHandler(err) {
		console.log(err ? err.message : 'unknown error occurred');
	}

	function getSearchResults(searchTerms, TERM) {
		var termSwitch = (parseInt(TERM,10) % 100 === 10);
		console.log("fall? "+termSwitch);
		var allCourses = termSwitch ? courseFallJSON : courseSpringJSON; // TODO
		var ret = [], i, j, course, term, filter, shouldPush;
		console.log(allCourses.length);
		for(i=0;i<allCourses.length;i++) {
			course = allCourses[i];
			shouldPush = false;
			for(j=0;j<searchTerms.length;j++) {
				term = searchTerms[j].text.toLowerCase();
				filter = searchTerms[j].filter;
				if(!term) continue;
				shouldPush = true;
				if(!(checkSubstring(term, filter, course, [Data.PROFESSOR, Data.NUMBER, Data.NAME, Data.CRN, Data.LONG_TITLE, Data.SHORT_TITLE, Data.SCHEDULE_DESC, Data.DESCRIPTION, Data.SUBJECT_CODE]) || 
					    convertStringToBlock(course[Data.SCHEDULE]) && convertStringToBlock(course[Data.SCHEDULE]) === convertStringToBlock(term))) {
					shouldPush = false;
					break;
				}
			}
			if(shouldPush) {
				ret.push(course);
			}
		}
		return ret;
	}

	function checkSubstring(term, filter, course, props) {
		var i, str;
		if(!props) {
			debugger;
		}
		for(i=0;i<props.length;i++){
			str = props[i] || "";
			//console.log('filter: '+filter+", prop = "+str);
			if((!filter || filter===str) && course[str] && (""+course[str]).toLowerCase().indexOf(term) >= 0) {
				return true;
			}
		}
		return false;
	}

	function excludeTerms(existing, exclude) {
		var ret = [], i, j, shouldPush, course, term;
		for(i=existing.length-1;i>=0;i--) {
			course = existing[i];
			shouldPush = true;
			for(j=0;j<exclude.length;j++) {
				term = exclude[j].text.toLowerCase();
				filter = exclude[j].filter;
				if(checkSubstring(term, filter, course, [Data.PROFESSOR, Data.NUMBER, Data.NAME, Data.CRN, Data.LONG_TITLE, Data.SHORT_TITLE, Data.SCHEDULE_DESC, Data.SUBJECT_CODE]) ||
					    convertStringToBlock(course[Data.SCHEDULE]) === convertStringToBlock(term)) {
					shouldPush = false;
					break;
				}
			}
			shouldPush && ret.push(course);
		}
		return ret;
	}

	function sortByHeader(courses, sortProp, ascending) {
		var sign = ascending ? 1 : -1;
		courses.sort(function(a,b){
			return a[sortProp] < b[sortProp] ? -1*sign : sign;
		});
	}

	function getCode(course) {
		var section = course[Data.SECTION] ? "-"+course[Data.SECTION] : '';
		return (course && course[Data.SUBJECT_CODE] && course[Data.NUMBER]) ? course[Data.SUBJECT_CODE] + course[Data.NUMBER] + section : "";
	}

	var that = {
		processCourses: processCourses,
		createTable: createTable,
		searchTerm: searchTerm,
		retrieveCourseData: retrieveCourseData,
		errorHandler: errorHandler,
		formatTime: formatTime,
		convertBlockHour: convertBlockHour,
		getConflicts: getConflicts,
		isConflicting: isConflicting,
		convertStringToBlock: convertStringToBlock,
		convertBlockToString: convertBlockToString,
		getSearchResults: getSearchResults,
		excludeTerms: excludeTerms,
		removeCourseFromList: removeCourseFromList,
		searchCourseInList: searchCourseInList,
		mapToBlock: mapToBlock,
		getCode: getCode
	}

	return that;

})();