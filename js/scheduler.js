/*********
 * js for scheduler ui creation and manipulation
 *********/

 var Scheduler = (function() { // put it all in a function to avoid conflicting global vars

 	var KEY_CODE_DELETE = 8,
 		KEY_CODE_TAB = 9
 		KEY_CODE_ENTER = 13,
 		KEY_CODE_LEFT = 37,
 		KEY_CODE_UP = 38,
 		KEY_CODE_RIGHT = 39,
 		KEY_CODE_DOWN = 40;

 	var CURRENT_TERM = 201310,
 		info = ["Status", "Code", "Title", "Professor", "Time", "Messages"];

 	var LEFT = 'left',
 	    RIGHT = 'right',
 	    UP = 'up',
 	    DOWN = 'down';

 	var MATCHED_PADDING = 2, // accounts for the padding on one side of a matched result
 		SEARCH_NEWLINE_THRESH = 30,
 		DEFAULT_CONTAINER_HEIGHT = 30,
 		DEFAULT_SEARCH_SECTION_HEIGHT = 40,
 		DEFAULT_MATCH_LIST_TOP = 30;

 	var SECTION_HEIGHTS = { // for section body animation
 		searchResultsSectionBody: 50,
 		cartSectionBody: 0
 	};

 	var TABLE_HEADERS = Data.tableHeaders,
 		TIMES = Data.times,
 		FALL_TITLES = Data.fall_titles,
 		SPRING_TITLES = Data.spring_titles,
 		SPRING_NUMBERS = Data.spring_course_number,
 		FALL_NUMBERS = Data.fall_course_number,
 		DEPARTMENTS = Data.departments,
		SUBJECTS = Data.subjects,
		SCHEDULES = Data.schedule,
 		FALL_PROFESSORS = Data.fall_professors,
 		SPRING_PROFESSORS = Data.spring_professors,
 		ALL_FALL_TERMS = TIMES.concat(SCHEDULES,SUBJECTS,FALL_PROFESSORS,FALL_TITLES,FALL_NUMBERS), // these are auto-complete terms
 		ALL_SPRING_TERMS = TIMES.concat(SCHEDULES,SUBJECTS,SPRING_PROFESSORS,SPRING_TITLES,SPRING_NUMBERS),
 		ALL_TERMS = ALL_FALL_TERMS;
 		SAVED_FALL_COURSES = Data.savedFallCourses;
 		SAVED_SPRING_COURSES = Data.savedSpringCourses;
 		REGISTERED_FALL_COURSES = Data.registeredFallCourses;
 		REGISTERED_SPRING_COURSES = Data.registeredSpringCourses;
 		SAVED_COURSES = SAVED_FALL_COURSES,
 		REGISTERED_COURSES = REGISTERED_FALL_COURSES;

 	var currentSelectedResult,
 	    currentSelectedAutoMatch;

	window.onload = load;

	function load() {
		Tests.searchTermTest();
		initScheduler();
	}

	/**
	 * do any initialization here
	 */
	function initScheduler() {
		$("#brownBox").width($('body').width()+"px");
		$("#tabContainer").width($('body').width()+"px");
		initHandlers();
		initSearch();
		createCart();
		showCart();
	}

	/**
	 * set up handlers for the scheduler
	 */
	function initHandlers() {
		// prevent arrow keys from scrolling entire page
		$(window).on('keydown', function(evt) {
			switch(evt.which) {
				case KEY_CODE_DOWN:
				case KEY_CODE_LEFT:
				case KEY_CODE_UP:
				case KEY_CODE_RIGHT:
					evt.preventDefault();
					break;
				default:
					break;
			}
		});

		// handler to toggle sections
		$('.sectionHeader').on('click', function(evt) {
			toggleTriangleImage($(this).find('.triangle')[0]);
			toggleSection($(this).parent());
		});

		// handler to expand search to include exclusions
		$('#expandSearch').on('click', expandSearch);

		$('#searchButton').on('click', function() {
			runSearch()
		});

		$('#selectTerm').on('change', function(evt) {
			CURRENT_TERM = parseInt(this.value, 10);
			if(CURRENT_TERM%100 === 10) {
				ALL_TERMS = ALL_FALL_TERMS;
				SAVED_COURSES = SAVED_FALL_COURSES;
				REGISTERED_COURSES = REGISTERED_FALL_COURSES;
			} else {
				ALL_TERMS = ALL_SPRING_TERMS;
				SAVED_COURSES = SAVED_SPRING_COURSES;
				REGISTERED_COURSES = REGISTERED_SPRING_COURSES;
			}
			$('.matchedResult').remove();
			placeTextarea($('#include'));
			placeTextarea($('#exclude'));
			placeResult("Independent Study", $('#exclude'));
			$('.classInfo').empty();
			noMatchesDiv = $(document.createElement('div')).attr('id', 'noSearchResults');
			noMatchesDiv.text('Use the search box above to generate search results.')
			$('.classInfo').append(noMatchesDiv);
			$('.searchInput').val('');
			hideCart();
			createCart();
			showCart();
			hideSearchResults();
			SECTION_HEIGHTS.searchResultsSectionBody = 50;
			showSearchResults();
			console.log("CURRENT_TERM: " + CURRENT_TERM);
		});
	}
	
	function fillAutoMatches(val, matchContainer) {
		var results,
			i,
			resultDiv;
		matchContainer.text('');
		matchContainer.empty();
		results = Utils.searchTerm(val);
		for(i=0;i<results.length;i++) {
			resultDiv = $(document.createElement('div'));
			resultDiv.addClass('autoMatchResult');
			resultDiv.text(results[i]);
			matchContainer.append(resultDiv);
		}
	}

	/**
	 * Set up handlers and such for search functionality
	 */
	function initSearch() {
		var include,
			exclude,
			includeContainer,
			excludeContainer,
			justSearched = false;
		include = $('#include');
		exclude = $('#exclude');
		includeContainer = $("#includeContainer");
		excludeContainer = $("#excludeContainer");
		placeTextarea(include);
		placeTextarea(exclude);
		//placeResult("mwf10-11",include);
		placeResult("Independent Study", exclude);

		noMatchesDiv = $(document.createElement('div')).attr('id', 'noSearchResults');
		noMatchesDiv.text('Use the search box above to generate search results.')
		$('.classInfo').append(noMatchesDiv);
		
		$('.searchInput').on('keydown', function(evt) {
			var inputBox = $(this);
			var value = inputBox.val();
			evt.stopPropagation();
			//fillAutoMatches(seachAutoMatches, inputBox.val());
			if(!value.trim()) {
				inputBox.val("");
				switch(evt.which) {
					case KEY_CODE_DELETE:
						removeMatched(inputBox);
						evt.preventDefault();
						break;
					case KEY_CODE_LEFT:
						moveNextMatched(inputBox,LEFT);
						break;
					case KEY_CODE_ENTER:
						runSearch();
						$("#"+inputBox.parent().attr('matches')).css('display', 'none');
						justSearched = true;
						evt.preventDefault();
						break;
					default:
						break;
				}
			} else {
				switch(evt.which) {
					case KEY_CODE_DOWN:
						moveNextAutoMatch(inputBox,DOWN);
						evt.preventDefault();
						break;
					case KEY_CODE_ENTER:
						runSearch();
						$("#"+inputBox.parent().attr('matches')).css('display', 'none');
						evt.preventDefault();
						justSearched = true;
						break;
					default:
						break;
				}
			}
		});
		$('.searchInput').on('focus', function() {
			currentSelectedResult = null;
		});

		$('.searchInput').on('keyup', function() {
			var inputBox = $(this);
			!justSearched && displayMatches(inputBox);
			justSearched = false;
		});
	}

	/**
	 * Uses current contents of search boxes to present correct search results
	 */
	function runSearch() {
		var searchTerms = [],
			excludeTerms = [],
			i,
			includeMatchDivs = $('#includeContainer').find('.matchedResult'),
			excludeMatchDivs = $('#excludeContainer').find('.matchedResult'),
			results, resultsText = [];

		for(i=0;i<includeMatchDivs.length;i++) {
			$(includeMatchDivs[i]).text().trim() && searchTerms.push({
				text: $(includeMatchDivs[i]).text().trim().toLowerCase(),
				filter: $(includeMatchDivs[i]).attr('filter')
			});
		}
		$("#include").val().trim() && searchTerms.push({
			text: $("#include").val().trim().toLowerCase(),
			filter: ""
		});
		for(i=0;i<excludeMatchDivs.length;i++) {
			$(excludeMatchDivs[i]).text().trim() && excludeTerms.push({
				text: $(excludeMatchDivs[i]).text().trim().toLowerCase(),
				filter: $(excludeMatchDivs[i]).attr('filter')
			});
		}
		$("#exclude").val().trim() && excludeTerms.push({
			text: $("#exclude").val().trim().toLowerCase(),
			filter: ""
		});

		results = Utils.getSearchResults(searchTerms, CURRENT_TERM);
		results = Utils.excludeTerms(results,excludeTerms);
		hideSearchResults();
		createSearchResultsTable(results);
		showSearchResults();
		return resultsText;
	}

	/**
     * Move within the auto match list
	 */
	function moveNextAutoMatch(inputBox, direction) {
		var autoMatches = $('#'+inputBox.parent().attr('matches')).children(),
		    next,
		    prev;
		if(!currentSelectedAutoMatch) {
			$(autoMatches[0]).focus(); // TODO use trigger or emit instead
		} else {
			if(direction === DOWN) {
	    		next = currentSelectedAutoMatch.next();
	    		if(next.is('.autoMatch')) {
	    			currentSelectedAutoMatch = next;
	    			next.focus();
	    		} // if already at the bottom, don't do anything
		    } else {
		    	prev = currentSelectedAutoMatch.prev();
		    	if(prev.is('.autoMatch')) {
	    			currentSelectedAutoMatch = prev;
	    		} else {
	    			currentSelectedAutoMatch = null;
	    			prev = inputBox;
	    		}
	    		prev.focus();
		    }
		}
	}

	/**
	 * Displays a list of candidate matches for the current search term
	 * @param inputBox     the relevant search box (include or exclude)
	 */
	function displayMatches(inputBox) {
	 	var term = inputBox.val().trim();
	 	var relatedMatches = $('#'+inputBox.parent().attr('matches'));
	 	var matches = Utils.searchTerm([term], ALL_TERMS);
	 	var i,
	 	    li;
	 	relatedMatches.empty();
	 	$('body').off('click');
	 	relatedMatches.off('click');
	 	if(matches.length > 0) {
	 		relatedMatches.css('display','block');
	 		relatedMatches.on('click', function(evt){
	 			evt.stopPropagation();
	 		});
	 		$('body').on('click', function(evt) {
	 			evt.stopPropagation();
	 			currentSelectedAutoMatch = null;
	 			relatedMatches.css('display', 'none');
	 			$('body').off('click');
	 		});
	 		for(i=0;i<matches.length;i++) {
	 			item = $(document.createElement('div'));
	 			item.attr('tabindex',-1);

	 			item.addClass('autoMatch');

	 			if (inputBox[0].id === 'include') {
	 				item.addClass('autoMatchInclude');
	 			} else {
	 				item.addClass('autoMatchExclude');
	 			}

	 			item.text(matches[i]);
	 			item.css('display', 'block');
	 			relatedMatches.append(item);

	 			item.on('focus', itemFocus(item, inputBox, relatedMatches));

	 			item.on('focusout', itemFocusOut(item));

	 			item.on('click', callPlaceAutoMatch(item, inputBox, relatedMatches));
	 		}
	 	} else {
	 		relatedMatches.css('display','none');
	 	}
	}

	function callPlaceAutoMatch(item, inputBox, relatedMatches) {
		return function(evt) {
			placeAutoMatch(item, inputBox, relatedMatches);
		}
	}

	function itemFocusOut(item) {
		return function(evt) {
			$(document).off('keydown');
		}
	}

	function itemFocus(item, inputBox, relatedMatches) {
		return function(evt) {
			currentSelectedAutoMatch = item;
			$(document).on('keydown', function(evt) {
				switch(evt.which) {
					case KEY_CODE_UP:
						moveNextAutoMatch(inputBox, UP);
						break;
					case KEY_CODE_DOWN:
						moveNextAutoMatch(inputBox, DOWN);
						break;
					case KEY_CODE_ENTER:
					case KEY_CODE_TAB:
						placeAutoMatch(item, inputBox, relatedMatches);
						evt.preventDefault();
						break;
					default:
						break
				}
			});
		}
	}

	function placeAutoMatch(item, inputBox, relatedMatches) {
		currentSelectedAutoMatch = null;
		var text = item.text();
		var pieces;
		var filter = '';
		if(text.match(/[A-Za-z][A-Za-z]\-\-[A-Za-z]/)) {
			pieces = text.split("--");
			text = pieces[0];
			filter = Data.SUBJECT_CODE;
		}
		placeResult(text, inputBox, filter);
		relatedMatches.empty();
		relatedMatches.css('display', 'none');
		$(document).off('keydown');
		inputBox.val('');
		inputBox.focus();
		//changeSearchAreaHeight(inputBox, true);
	}

	/**
	  * Moves to the next matching result in the given direction
	  * Assume this has been called when:
	  *    - we want to move left to the last match from the search box
	  *    - we want to move right or left within the list of matches
	  * @param inputBox    the relevant search box
	  * @param direction   either LEFT or RIGHT
	  */
	function moveNextMatched(inputBox, direction) {
	    var results = inputBox.parent().find('.matchedResult'),
	    	next,
	    	prev;
	    if(!currentSelectedResult) {
	    	currentSelectedResult = $(results[results.length - 1]);
	    	currentSelectedResult.focus();
	    } else {
	    	if(direction === RIGHT) {
	    		next = currentSelectedResult.next();
	    		if(next.is('.matchedResult')) {
	    			currentSelectedResult = next;
	    		} else {
	    			currentSelectedResult = null;
	    			next = inputBox;
	    		}
		    	next.focus();
		    } else {
		    	prev = currentSelectedResult.prev();
		    	if(prev.is('.matchedResult')) {
	    			currentSelectedResult = prev;
	    			prev.focus();
	    		}
		    }
	    }
	}



	/**
	 * Removes the last matched result if there is one
	 * @param inputBox     the relevant search box (include or exclude)
	 * @param matched      (optional) the matched element to remove
	 */
	function removeMatched(inputBox, matched) {
	 	var i,
	 	    matchingText=[],
	 	    index;
	 	if(matched) {
	 		index = matched.parent().find('.matchedResult').index(matched);
	 		$(matched).remove();

	 		matchedElts = inputBox.parent().find('.matchedResult');
	 		for(i=0; i<matchedElts.length; i++) {
	 			matchingText.push($(matchedElts[i]).text());
	 			$(matchedElts[i]).remove();
	 		}
	 		inputBox.css({
	 			left: "0px",
	 			top: "0px"
	 		});
	 		//placeTextarea(inputBox);
	 		for(i=0; i<matchedElts.length;i++) {
	 			placeResult(matchingText[i], inputBox);
	 			//debugger;
	 		}
	 		matchedElts = inputBox.parent().find('.matchedResult');
	 		for(i=0; i<matchedElts.length;i++) {
	 			$(matchedElts[i]).text(matchingText[i]);
	 		}
	 		if(matchedElts.length > index) {
	 			next = $(matchedElts[index]);
	 			currentSelectedResult = next;
	 		} else {
	 			currentSelectedResult = null;
	 			next = inputBox;
	 		}
	    	next.focus();
	 	} else {
	 		matchedElts = inputBox.parent().find('.matchedResult');
	 		if(matchedElts.length > 0) {
	 			$(matchedElts[matchedElts.length-1]).remove();
	 		}
	 		placeTextarea(inputBox);
	 	}
	 	
	}

	/**
	 * Places a result box in the search input container
	 * @param result     the value of the search result
	 * @param inputBox   the relevant search box (include or exclude)
	 */
	function placeResult(result, inputBox, filter) {
		var matchedResult;
		var newLeft, newTop;
		filter = filter || "";
		var parentWidth = inputBox.parent().width();
		var existingMatches = $('.matchedResult');
		var currLoc = {
			left: parseFloat(inputBox.css('left')),
			top: parseFloat(inputBox.css('top'))
		}
		// var exceedsSpace = lastLeft + lastWidth + parentWidth - lastLeft - lastWidth + 
		matchedResult = $(document.createElement('div'));
		matchedResult.attr('tabindex', -1)
					 .attr('filter', filter)
		             .addClass('matchedResult')
		             .css({
		             	'left': 0 + "px", // just to get full width
		             	'top': 0 + "px"
		             }).text(result);
		inputBox.parent().append(matchedResult);
		if(parentWidth - currLoc.left - matchedResult.width() < 10) {
			matchedResult.css({
				'top': currLoc.top + 30 + "px"
			});
		} else {
			matchedResult.css(currLoc);
		}
		matchedResult.on('focus', function() {
			currentSelectedResult = matchedResult;
			matchedResult.on('keydown', function(evt) {
				switch(evt.which) {
					case KEY_CODE_DELETE:
						removeMatched(inputBox, matchedResult);
						evt.preventDefault();
						break;
					case KEY_CODE_LEFT:
						moveNextMatched(inputBox, LEFT);
						break;
					case KEY_CODE_RIGHT:
						moveNextMatched(inputBox, RIGHT);
						break;
					default:
						break;
				}
			});
		});
		matchedResult.on('focusout', function() {
			matchedResult.off('keydown');
		});

		
		placeTextarea(inputBox);
	}
	
	var OBJECT_SIZE = 6;

 	function getTimeBounds(courses) {
 		var i, j, k, ret = {
 			max: -Infinity,
 			min: Infinity
 		}, times;
 		if(courses) {
 			for(i=0; i<courses.length; i++) {
 				times = Utils.convertBlockHour(Utils.convertStringToBlock(courses[i][Data.SCHEDULE]));
 				
 				for(j=0;j<times.length;j++) {
 					for(k=0;k<times[j].length;k++) {
 						if(k%2 === 1 && times[j][k] > ret.max) {
 							ret.max = Math.ceil(times[j][k])+1;
 						} else if(k%2 === 0 && times[j][k] < ret.min) {
 							ret.min = Math.floor(times[j][k])-1;
 						}
 					}
 				}
 			}
 		}
 		return ret;
 	}

 	/**
	 * Creates a table representing the user's cart.
	 * Uses a table to represent the grid, overlays divs to represent course blocks.
	 * @param isMini       indicates whether we are creating a mini scheduler
	 */
 	function createCart(isMini) {
 		var min = Infinity,
 			max = -Infinity,
			days = ["", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday"],
			array = array || [],
			t, k, i,
			table,
			savedAndRegistered = SAVED_COURSES.concat(REGISTERED_COURSES);

		// TODO use our final course object format to determine
		//      what the actual max and min should be
		timeBounds = getTimeBounds(savedAndRegistered);
		max = timeBounds.max;
		min = timeBounds.min;

		max = (max === -Infinity) ? 15 : max;
		min = (min === Infinity) ? 9 : min;

		var tdHeight = 60;

		table = Utils.createTable({
			headers: days,
			numRows: max - min,
			startTime: min
		});
		$("#scheduler").empty();
		$("#scheduler").append(table);

		table.attr('id', 'cart');

		overlayCourses({
			table: table,
			saved: SAVED_COURSES,
			registered: REGISTERED_COURSES,
			tdHeight: tdHeight
		});

		SECTION_HEIGHTS.cartSectionBody = (max - min + 1)*tdHeight + 20; // 20 for a bit of padding
		//debugger;
 	}

 	function overlayCourses(obj) {
 		var table = obj.table,
 		    saved = obj.saved,
 		    tdHeight = obj.tdHeight,
 		    registered = obj.registered,
 		    i, times, j, k, n,
 		    startTime = parseFloat(table.attr('startTime')),
 		    stopTime = parseFloat(table.attr('stopTime')),
 		    height = (stopTime - startTime) * tdHeight,
 		    courseElt, divHeight, divTop,
 		    width = $(table.find('td')[0]).width(),
 		    headerHeight = $(table.find('th')[0]).height(),
 		    className, innerElt, doesConflict, conflicts,
 		    allAdded = registered.concat(saved),
 		    temp, conflictDetails, status;

 		for(i=0;i<allAdded.length;i++) {
 			times = Utils.convertBlockHour(Utils.convertStringToBlock(allAdded[i][Data.SCHEDULE]));
 			conflicts = Utils.getConflicts(allAdded[i], allAdded);
 			doesConflict = conflicts.length > 0;

 			className = !doesConflict ? (i<registered.length ? 'registeredCourse' : 'savedCourse') : 'conflictingCourse';
 			for(j=1;j<6;j++) {
 				for(k=0;k<times[j-1].length;k+=2) {
 					divTop = (times[j-1][k] - startTime) * tdHeight + headerHeight + 1;
 					divHeight = (times[j-1][k+1] - times[j-1][k]) * tdHeight;
 					//debugger;
 					courseElt = $(document.createElement('div'));
 					courseElt.addClass(className);
 					courseElt.addClass('cartCourse');
 					courseElt.css({
 						top: divTop + "px",
 						height: divHeight + "px",
 						width: (100/6 - 0.5) + "%",
 						left: (100*j/6 +0.13) + "%"
 					});
 					innerElt = $(document.createElement('div'));
 					innerElt.addClass('cartCourseInner');

 					status = $(document.createElement('img'));
 					if (className === 'savedCourse') {
	 					status[0].src = "../images/checkmark.png";
	 				} else if (className === 'registeredCourse') {
	 					status[0].src = "../images/lock.png";
	 				} else {
	 					status[0].src = "../images/lock.png";
	 				}
	 				status.css('display','inline-block');
	 				$(courseElt).append(status);

 					var listOfConflicts = "<br /><br />"+Utils.getCode(allAdded[i]);
 					for(n=0;n<conflicts.length;n++) {
						listOfConflicts += "<br />"+Utils.getCode(conflicts[n]);
					}

					var location = allAdded[i][Data.BUILDING] || "";
					var room = allAdded[i][Data.ROOM] || "";
					var meetingPlace = room.replace(/\'/g,'') + ", " + location;
					innerElt.html(doesConflict ? 'Conflict:' + listOfConflicts : Utils.getCode(allAdded[i]) + "<br>" + allAdded[i][Data.NAME] + "<br>" +  meetingPlace);
 					// innerElt.html(doesConflict ? 'Conflict:' + listOfConflicts : Utils.getCode(allAdded[i]) + "<br>" + allAdded[i][Data.NAME] + "<br>" + Utils.convertBlockToString(Utils.convertStringToBlock(allAdded[i][Data.SCHEDULE])) );
 					courseElt.append(innerElt);
 					table.parent().append(courseElt);

 					if(doesConflict) {
 						courseElt.on('mouseenter', mouseEnter(allAdded[i], conflicts, conflictDetails, courseElt));

 						courseElt.on('mouseleave', mouseLeave());
 					} else {
 						courseElt.on('click', showPopup(allAdded[i]));
 					}
 				}
 			}
 		}
 	}

 	function mouseEnter(course, conflicts, conflictDetails, courseElt) {
 		return function(evt) {
 			var htmlstr;
 			evt.stopPropagation();
			conflictDetails = $(document.createElement('div')).addClass('conflictDetails');
			htmlstr = "Conflicting courses:<br /><br /><ul>";
			htmlstr += "<li><div code='"+course[Data.CRN]+"' class='conflictCourseSpan'>"+Utils.getCode(course)+" <button class='removeIfConflict' type='button' code='"+course[Data.CRN]+"'>Remove</button></div> </li>";
			for(n=0;n<conflicts.length;n++) {
				htmlstr += "<li><div code='"+conflicts[n][Data.CRN]+"' class='conflictCourseSpan'>"+Utils.getCode(conflicts[n])+" <button class='removeIfConflict' type='button' code='"+conflicts[n][Data.CRN]+"'>Remove</button></div> </li>";
			}
			htmlstr += "</ul>";
			conflictDetails.html(htmlstr);
			courseElt.append(conflictDetails);
			$('.conflictDetails button').on('click', function(evt) {
				removeFromCart($(this).attr('code'));
			});
			$('.conflictCourseSpan').on('click', function(elt) {
				var allInCart = SAVED_COURSES.concat(REGISTERED_COURSES), i;
				for(i=0;i<allInCart.length;i++) {
					if(parseInt($(this).attr('code'),10) === parseInt(allInCart[i][Data.CRN],10)) {
						popUp(allInCart[i]);
						break;
					}
				}
			});
 		}
 	}

 	function removeFromCart(crn) {
 		var i;
 		for(i=0;i<SAVED_COURSES.length;i++) {
 			if(parseInt(SAVED_COURSES[i][Data.CRN],10) === parseInt(crn)) {
 				SAVED_COURSES.splice(i,1);
 				createCart();
 				return;
 			}
 		}
 		for(i=0;i<REGISTERED_COURSES.length;i++) {
 			if(parseInt(REGISTERED_COURSES[i][Data.CRN],10) === parseInt(crn,10)) {
 				REGISTERED_COURSES.splice(i,1);
 				createCart();
 				return;
 			}
 		}
 		createCart();
 	}

 	function mouseLeave(conflictDetails) {
 		return function(evt) {
 			evt.stopPropagation();
 			$('.conflictDetails').remove();
 		}
 	}

	function createSearchResultsTable(array){
		var table,
			tableHead,
			tableRow,
			i, j, h, n, ii,
			days,
			currObj,
			currDay,
			beginTime,
			start,
			endTime,
			end,
			rows,
			CourseDetailArray,
			Course,
			noMatchesDiv;

		$('.classInfo').empty();

		if(!array || array.length === 0) {
			noMatchesDiv = $(document.createElement('div')).attr('id', 'noSearchResults');
			noMatchesDiv.text('No matching courses found.');
			$('.classInfo').append(noMatchesDiv);
			SECTION_HEIGHTS.searchResultsSectionBody = 50;
			return;
		}		

	 	table = document.createElement('table');
	 	$(table).attr('id', 'resultsTable');
	 	tableHead = document.createElement('thead');
	 	tableRow = document.createElement('tr');
	 	$(tableHead).append(tableRow);
	 	table.cellPadding = "0";
	 	table.zIndex = 0;
	 	
	 	/*Create table headers*/
	 	for(i=0; i < info.length;i++) {
	 		tableHeader = $(document.createElement('th'));
	 		$(tableRow).append(tableHeader);
	 		tableHeader.attr('id', info[i]);
	 		tableHeader.addClass('tableHead');
 			tableHeader.css('background-color', '#EDCAB9');
 			tableHeader.attr('sortingBy', 0);
	 		tableHeader.text(info[i]);

	 		arrow = $(document.createElement('img'));
	 		arrow[0].src = "../images/table_header_down_arrow.gif";
	 		arrow.css('display', 'inline-block');
	 		$(tableHeader).append(arrow);

	 		tableHeader.on('click', callSortResults(array, info[i]));
	 	}
	 	
	 	$(table).append(tableHead);

	 	tableBody = $(document.createElement('tbody'));
	 	$(table).append(tableBody);
	 	$('.classInfo').append(table);

	 	var rowHeight = 25;

	 	callSortResults(array, "Code")();
	 	SECTION_HEIGHTS.searchResultsSectionBody = Math.min(rowHeight*(array.length+1), 500) + 30;
	}

	function fillTableBody(array) {
		var i, j, n, t, tableEntry,
		    tableBody = $($('#resultsTable').find('tbody')[0]);
		tableBody.empty()
		for(i=0; i < array.length; i++) {
	 		tableRow = document.createElement('tr');
	 		
	 		tableBody.append(tableRow);
	 				
	 		for(j = 0; j < OBJECT_SIZE; j++){
	 			tableEntry = document.createElement('td');
	 			tableEntry.style.width = "100px";
	 			tableEntry.style.borderTop = "1px solid black";
	 			tableEntry.style.borderLeft = "1px solid black";
				
				if(info[j] === "Status"){
					t = " ";
					for(ii=0;ii<SAVED_COURSES.length;ii++) {
						if(array[i][Data.CRN] === SAVED_COURSES[ii][Data.CRN]) {
							t = 'In Cart';
						}
					}
					for(ii=0;ii<REGISTERED_COURSES.length;ii++) {
						if(array[i][Data.CRN] === REGISTERED_COURSES[ii][Data.CRN]) {
							t = 'Registered';
						}
					}
					$(tableEntry).text(t);
				}
				else if(info[j] === "Code"){
					$(tableEntry).text(array[i][Data.SUBJECT_CODE] + array[i][Data.NUMBER]);
				}
				else if(info[j] === "Professor") {
					$(tableEntry).text(array[i][Data.PROFESSOR]); 
				}
				else if(info[j] === "Title"){
					$(tableEntry).text(array[i][Data.NAME]);
				}
				else if(info[j] === "Time"){
					$(tableEntry).text(Utils.convertBlockToString(Utils.convertStringToBlock(array[i][Data.SCHEDULE])));
				}
				else if(info[j] === "Messages"){
					t = getMessage(array[i]);
					$(tableEntry).append(t ? t : " ");
				}
				$(tableRow).append(tableEntry);	
			}
		}
		var rows = tableBody.parent()[0].getElementsByTagName("tr");
		var CourseDetailArray = new Array(array.length);
		for(h = 0; h < array.length; h++){
			CourseDetailArray[h] = array[h];
		}
		var row;
	    for (n = 1; n < rows.length; n++) {
	        row = $(rows[n]);
	        row.on('click', function () {
		    	var Course = CourseDetailArray[this.rowIndex-1];
		    	popUp(Course);         
	        });
	    }
	}

	function callSortResults(array, sortProp) {
		return function(evt) {
			var headerElt = $('#'+sortProp);
			    oldSort = parseInt(headerElt.attr('sortingBy'),10),
				newSort = oldSort%2 + 1; // sends unsorted/descending --> ascending, ascending --> descending
			$('.tableHead').attr('sortingBy',0);
			headerElt.attr('sortingBy', newSort);
			sortAscending = newSort === 1 ? true : false;
			$('.tableHead').css('background-color', '#CDCAB9')
			$('.tableHead').find('img').css('visibility', 'hidden');

			headerElt.css('background-color', sortAscending ? '#EBEAE3' : '#908D82');
			
			headerElt.find('img').css('visibility', 'visible');
			headerElt.find('img')[0].src = sortAscending ? "../images/table_header_up_arrow.gif" : "../images/table_header_down_arrow.gif";

			sortResults(array,sortProp,sortAscending);
		}
	}

	function sortResults(array, sortProp, sortAscending) {
		var sign = sortAscending ? 1 : -1;
		array.sort(function(a,b) {
			var comp;
			switch(sortProp) {
				case "Status":
					comp = getStatus(a) < getStatus(b);
					break;
				case "Code":
					comp = a[Data.SUBJECT_CODE]+a[Data.NUMBER] < b[Data.SUBJECT_CODE]+b[Data.NUMBER];
					break;
				case "Title":
					comp = a[Data.NAME] < b[Data.NAME];
					break;
				case "Time":
					comp = Utils.convertBlockToString(Utils.convertStringToBlock(a[Data.SCHEDULE])) < Utils.convertBlockToString(Utils.convertStringToBlock(b[Data.SCHEDULE]));
					break;
				case "Professor":
					comp = a[Data.PROFESSOR] < b[Data.PROFESSOR];
					break;
				case "Messages":
					comp = getMessage(a) < getMessage(b);
					break;
				default:
					return a < b;
			}
			return comp ? -sign : sign;
		});
		fillTableBody(array);
	}

	function getStatus(course) {
		var isSaved = Utils.searchCourseInList(course, SAVED_COURSES);
	    if(isSaved) {
	    	return "In Cart";
	    } else {
	    	return Utils.searchCourseInList(course, REGISTERED_COURSES) ? "Registered" : "";
	    }
	}

	function getMessage(course) {
		var message = "";
		if(parseFloat(course[Data.ACTUAL_ENRL]) >= parseFloat(course[Data.CAP])) {
			message = "Full. ";
		}
		if(Utils.getConflicts(course, SAVED_COURSES.concat(REGISTERED_COURSES)).length > 0) {
			message += "Conflicting. ";
		}
		// TODO: deal with pre/coreqs
		return message;
	}

	function showPopup(courseObj) {
		return function(evt) {
			popUp(courseObj);
		}
	}
	
	function popUp(courseObj){
		$('.popupOverlay').remove(); // remove any existing overlays
		
		var messagesDiv = $(document.createElement('div'));
		var messages = getMessage(courseObj);
		messagesDiv.text(messages);
		messagesDiv.attr('id', 'popupMessages');
		//var isConflicting = messages.toLowerCase().match(/confl/);
		
		var meetingPlace = "";
		var locationDiv = $(document.createElement('div'));
		var location = courseObj[Data.BUILDING] || "";
		var room = courseObj[Data.ROOM] || "";
		meetingPlace = location + " " + room.replace(/\'/g,'');
		//Should probably have a comma between room and location, but only if they aren't null
		locationDiv.text(meetingPlace);
		locationDiv.attr('id', 'popupBuilding');
		
		var profDiv = $(document.createElement('div'));
		var prof = courseObj[Data.PROFESSOR] || "";
		profDiv.text(prof);
		profDiv.attr('id', 'popupProf');
		
		var CourseInfoDiv = $(document.createElement('div'));
		var details = courseObj[Data.DESCRIPTION] || "";
		CourseInfoDiv.html(details);
		CourseInfoDiv.addClass('courseDescriptionPopup');
		CourseInfoDiv.attr('id', 'popupCourseInfo');
		
		// var CRNDiv = $(document.createElement('div'));
		// CRNDiv.text(code);
		// CRNDiv.attr('id', 'popupCode');
		
		var code = Utils.getCode(courseObj) || "";
		var name = courseObj[Data.LONG_TITLE] || "";
		var CourseNameDiv =  $(document.createElement('div'));
		CourseNameDiv.text(code + ": " + name);
		CourseNameDiv.attr('id', 'popupCourseName');
		
		var ExamDiv = $(document.createElement('div'));
		var ExamInfo = "Exam Info: " + (courseObj[Data.EXAM] || "");
		ExamDiv.text(ExamInfo);
		ExamDiv.attr('id', 'popupExam');
		

		var term = $(document.createElement('div'));
		var termInfo = courseObj[Data.TERM];
		term.text(termInfo);
		term.attr('id', 'popupTerm');

		var MeetingTimeDiv = $(document.createElement('div'));
		var MeetingTimeInfo = Utils.convertBlockToString(Utils.convertStringToBlock(courseObj[Data.SCHEDULE]));
		MeetingTimeDiv.text(MeetingTimeInfo);
		MeetingTimeDiv.attr('id', 'popupMeetingTime');
		
		var overlay = $(document.createElement('div'));
		overlay.on('click', function(evt){
			overlay.remove();
		});
		overlay.addClass('popupOverlay');
		overlay.attr('id', 'popupOverlay');
		$('body').prepend(overlay);
	    var popup = $(document.createElement('div'));
	    popup.on('click', function(evt){
	    	evt.stopPropagation();
	    });
	    popup.addClass('popup');
	    popup.attr('id', 'test');
	    var cancel = $(document.createElement('div'));
	    cancel.addClass('cancel');
	    cancel.text('X');
	    cancel.on('click', function () {
	    	overlay.remove();
	    });


	    var isSaved = Utils.searchCourseInList(courseObj, SAVED_COURSES);
	    var isRegistered = Utils.searchCourseInList(courseObj, REGISTERED_COURSES);

	    var cartButton = $(document.createElement('button'));
	    cartButton.addClass('cartButton');
	    cartButton.text(isSaved ? 'Remove from Cart' : 'Save in Cart');
	    cartButton.on('click',function() {
	    	popUp(courseObj);
	    	if(isSaved) {
	    		removeFromCart(courseObj[Data.CRN]);
	    		popUp(courseObj);
	    	} else {
	    		SAVED_COURSES.push(courseObj);
	    		createCart();
	    		popUp(courseObj);
	    	}
	    });


	    var registerButton = $(document.createElement('button'));
	    registerButton.addClass('registerCourseButton');
	    registerButton.text(isRegistered ? 'Drop Course' : 'Register');
	    registerButton.attr('id', isRegistered);
	    registerButton.on('click', function() {
	    	$('#popupOverlay').remove();
	    	//popUp(courseObj);
	    	if (isRegistered) {
	    		confirmationPopup("Are you sure you want to drop this course?", "Yes", "No", function() {
	    			removeFromCart(courseObj[Data.CRN]);
	    		});
	    	} else {
	    		if(isSaved) {
	    			Utils.removeCourseFromList(courseObj, SAVED_COURSES);
	    		}
	    		REGISTERED_COURSES.push(courseObj);
	    		createCart();
	    		popUp(courseObj);
	    	}
	    });

		popup.append(cancel);
		// popup.append(CRNDiv);
		popup.append(CourseNameDiv);	
		popup.append(profDiv);	
		popup.append(locationDiv);
		popup.append(MeetingTimeDiv);
		popup.append(ExamDiv);
		popup.append(CourseInfoDiv);
		popup.append(messagesDiv);
		//popup.append(ExamDiv);
		
		//popup.append(term);
	   
	    !isRegistered && popup.append(cartButton); // don't include
	    if(!((messages || REGISTERED_COURSES.length >= 5) && !isRegistered)) {
	    	popup.append(registerButton);
	    }
	    overlay.append(popup);
	}

	// confirm submission
	function confirmationPopup(message, confirmButtonText, cancelButtonText, confirmCallback) {
		confirmButtonText = confirmButtonText || "Continue";
		cancelButtonText = cancelButtonText || "Cancel";
		confirmCallback = confirmCallback || function() { overlay.remove() };

		var overlay = $(document.createElement('div'));
		overlay.on('click', function(evt){
			overlay.remove();
		});
		overlay.addClass('popupOverlay');

		var popup = $(document.createElement('div'));
	    popup.on('click', function(evt){
	    	evt.stopPropagation();
	    });
	   // popup.addClass('popup');
	    popup.addClass('confirmationPopupMessage'); // can use this class to style general confirmation popups

	    var popupMessage = $(document.createElement('div'));
	    popupMessage.addClass('popupMessage');
	    popupMessage.text(message);

	    var cancelButton = $(document.createElement('button'));
	    cancelButton.addClass('popupCancelButton');
	    cancelButton.text(cancelButtonText);
	    cancelButton.on('click', function() {
	    	overlay.remove();
	    });

	    var confirmButton = $(document.createElement('button'));
	    confirmButton.addClass('popupConfirmButton');
	    confirmButton.text(confirmButtonText);
	    confirmButton.on('click', function() {
	    	confirmCallback();
	    	overlay.remove();
	    });

		popup.append(popupMessage);
	    popup.append(cancelButton);
	    popup.append(confirmButton);
	    overlay.append(popup);
	    $('body').prepend(overlay);
	}

	// can be used as a callback
	function callRemoveFromCart(code) {
		return function() {
			removeFromCart(code);
		}
	}

	/**
	 * sets the width and left properties of a textarea element to fill available space
	 * @param area     textarea element
	 */
	function placeTextarea(area) {
		var parentWidth,
			siblings,
			sibling,
			siblingsWidth = 0,
			i, availableWidth, newTop;
		area = $(area); // make sure it's a jQuery object
		var oldTop = 0;
		parentWidth = area.parent().width();
		siblings = area.siblings();
		for(i=0; i<siblings.length; i++) {
			sibling = $(siblings[i]);
			if(parseFloat(sibling.css('top')) > oldTop){
				siblingsWidth = 0;
				oldTop = parseFloat(sibling.css('top'));
			}
			siblingsWidth += sibling.width() + parseFloat(sibling.css('margin-left')) + 2*MATCHED_PADDING;
			if(i === siblings.length - 1) {
				newTop = parseFloat(sibling.css('top'));
			}
		}
		availableWidth = (parentWidth - siblingsWidth - parseFloat(area.css('margin-left')));
		newLeft = siblingsWidth;
		if(availableWidth < SEARCH_NEWLINE_THRESH) { 
			availableWidth = parentWidth - parseFloat(area.css('margin-left'));
			newTop = newTop + 30; // TODO hard-coded for now
			newLeft = 0;
		} 
		area.css({
			'width': availableWidth + "px",
			'left': newLeft + "px",
			'top': newTop + "px"
		});

		autoSizeSearchContainer(area);
	}

	/**
	 * toggles between a right triangle and a down triangle
	 * @param triangle      HTML image element for triangle
	 */
	function toggleTriangleImage(triangle) {
		var right,
			down;
		right = '../images/rightTriangle.svg';
		down = '../images/downTriangle.svg';
		triangle.src = triangle.src.match(/down/) ? right : down;
	}
	
	
	/**
	 * expand search region to include the 'exclude' box
	 */
	function expandSearch() {
		$('#expandSearch').css('display','none'); // hide the expand triangle button
		placeTextarea($('#exclude'));
		$('#excludeContainer').animate({ // animate the exclude search box's opacity to fade in
			'opacity': 1,
		}, 200);
		$('#includeContainer').animate({ // animate include box container
			'left': '19%'
		}, 200);
		$('#includeAutoMatches').animate({
			'left': '19%'
		}, 200);
		$('#searchButtonContainer').animate({ // animate search button
			'left': '82%'
		}, 200);
	}
	
	
	/**
	 * Expand or minimize a section
	 * @param section      jQuery object of section to toggle
	 */
	function toggleSection(section) {
		var sectionBody = $(section.find('.sectionBody'));
		var currentDisplay = sectionBody.css('display');
		var isExpanding = currentDisplay === 'none';
		isExpanding && sectionBody.css('display', 'block');
		//!isExpanding && sectionBody.css('display', 'none');
		console.log(SECTION_HEIGHTS[sectionBody.attr('id')]);
		sectionBody.stop();
		sectionBody.animate({
			height: (isExpanding ? SECTION_HEIGHTS[sectionBody.attr('id')] : 0) + "px"
		}, 200, function() {
			!isExpanding && sectionBody.css('display', 'none');
		});
	}

	function showSearchResults() {
		if($("#searchResultsSectionBody").css('display') === 'none') {
			toggleTriangleImage($("#searchResultsTriangle")[0]);
			toggleSection($("#searchResultsSection"));
		}
	}

	function hideSearchResults() {
		if($("#searchResultsSectionBody").css('display') !== 'none') {
			toggleTriangleImage($("#searchResultsTriangle")[0]);
			$("#searchResultsSectionBody").css({
				display: 'none',
				height: '0px'
			});
		}
	}

	function showCart() {
		if($("#cartSectionBody").css('display') === 'none') {
			toggleTriangleImage($("#cartTriangle")[0]);
			toggleSection($("#cartSection"));
		}
	}

	function hideCart() {
		if($("#cartSectionBody").css('display') !== 'none') {
			toggleTriangleImage($("#cartTriangle")[0]);
			$("#cartSectionBody").css({
				display: 'none',
				height: '0px'
			});
		}
	}

	function autoSizeSearchContainer(inputBox) {
		var currLoc = {
				top: parseFloat(inputBox.css('top')),
				height: inputBox.height()
			},
			container = inputBox.parent(),
			searchArea = $('#searchBoxContainer'),
			matches = $("#"+container.attr('matches'));
		var oldContainerHeight = container.height() - 4;
		var oldSearchArea = searchArea.height();
		var newContainerHeight = currLoc.top + DEFAULT_CONTAINER_HEIGHT;
		container.css({
			height: (newContainerHeight) + "px"
		});
		searchArea.css({
			height: DEFAULT_SEARCH_SECTION_HEIGHT + (newContainerHeight - DEFAULT_CONTAINER_HEIGHT) + "px"
		});
		matches.css({
			top: DEFAULT_MATCH_LIST_TOP + (newContainerHeight - DEFAULT_CONTAINER_HEIGHT) + "px"
		});


	}


	/**
     * If the user types too many search terms, extend the search results area vertically
     * @param inputBox    the relevant search box
     * @param expand      boolean value; if true, expand search area by 30px, if false, contract by 30px
	 */
	// function changeSearchAreaHeight(inputBox, expand) {
	// 	var currentHeight = inputBox.parent().height();
	// 	var currentContainerHeight = $("#searchBoxContainer").height();
	// 	var currentTop = parseInt(inputBox.css('margin-top'), 10);
	// 	var sign = expand ? 1 : -1;
	// 	inputBox.parent().css('height', (currentHeight + sign*35) + "px");
	// 	$("#searchBoxContainer").css('height', (currentContainerHeight + sign*35) + "px");
	// 	inputBox.css('margin-top', (currentTop + sign*35) + "px");
	// 	inputBox.css('left', "0px");
	// }

	var that = {
		/* anything that needs to be used in other js files should be
		   included here, e.g.:

		toggleSection: toggleSection
		
		   and referenced from other file using Scheduler.toggleSection */
	}

	return that;

})();
