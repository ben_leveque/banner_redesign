var Tests = (function() {
	/**
	 * Test cases for the searchTerm function
	 */
	function searchTermTest() {
		var i,
			results,
			errors=0,
			prevErrors,
			terms;
		terms = [
			'cs', 'cs195i', 'cs195n', 'cs195e',
			'cs195u', 'ucs195', 'cs lewis',
			'csci 195i', 'csci195i', 'mwf9-10',
			'h hour', 'tth2-3', 'andries van dam',
			'jeff huang', 'steven reiss', 'steve reiss',
			'ui design', '1000-level', 'independent study'
		];
		
		// basic: make sure searching for each term in the list
		//        gives a nontrivial response
		
		prevErrors=0;
		for(i=0;i<terms.length;i++) {
			results = Utils.searchTerm(terms[i],terms);
			if(results.indexOf(terms[i]) < 0) {
				errors++;
				console.log("searchTermTest (basic) failed at index "+i);
			}
		}
		if(errors === prevErrors) {
			console.log("searchTermTest (basic) passed");
		}
		prevErrors = errors;
		
		// more tests
		//
	}

	function processCoursesTest() {
		var results,
			i,
			j,
			currBlock = -Infinity,
			isValid = true;
		var courses = [ {
			Title: 'course a',
			Block: 1
		}, {
			Title: 'course b',
			Block: 0
		}, {
			Title: 'course c',
			Block: 3
		}, {
			Title: 'course d',
			Block: 1
		}, {
			Title: 'course e',
			Block: 0
		}, {
			Title: 'course f',
			Block: 10
		}, {
			Title: 'course g',
			Block: 4
		} ];

		results = Utils.processCourses(courses);
		
		for(i=0;i<results.length;i++){
			if (results[i][0].Block <= currBlock) {
				isValid = false;
			}
			currBlock = results[i][0].Block;
			for(j=0;j<results[i].length;j++) {
				if(results[i][j].Block !== currBlock) {
					isValid = false;
				}
			}
		}

		console.log("processCoursesTest "+(isValid ? "passed" : "failed"));
	}

	var that = {
		searchTermTest: searchTermTest,
		processCoursesTest: processCoursesTest
	}

	return that;
})();